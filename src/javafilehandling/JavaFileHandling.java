package javafilehandling;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class JavaFileHandling {

    private static BufferedReader br = null;
    private static PrintWriter out = null;

    public static void main(String[] args) throws IOException {
        writeFile();
        readFile();
    }

    public static void writeFile() throws IOException {
        try {
            out = new PrintWriter(new FileWriter("file.txt"));
            out.println("1 asd asdfdf");
            out.println("2 sdffe sdfdf");
        } finally {
            out.close();
        }
    }

    public static void readFile() throws FileNotFoundException, IOException {
//        try{
//            br =new BufferedReader(new FileReader("file.txt"));
//            String line;
//            while((line=br.readLine())!=null){
//                System.out.println(line);
//            }
//        }finally{
//            br.close();
//        



        Scanner scn = new Scanner(new FileInputStream("file.txt"));
        while (scn.hasNext()) {
            int id = scn.nextInt();
            String name = scn.next();
            String address = scn.next();

            System.out.println("Id:" + id + "\t\tName:" + name+"\tAddress:"+address);
        }
    }

}
