package javafilehandling;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class QuestionTwo {

    public static void main(String[] args) throws IOException {
        //writeData();
        readData();
        readSalGreaterTwoK();
    }

    private static void writeData() throws IOException {
        PrintWriter out = null;
        Scanner in = new Scanner(System.in);
        try {
            out = new PrintWriter(new FileWriter("emp.doc"));
            for (int i = 0; i < 5; i++) {
                System.out.println("Enter record #" + (i + 1));

                System.out.print("Enter Id:");
                int id = in.nextInt();
                System.out.print("Enter name:");
                String name = in.next();
                System.out.print("Enter address:");
                String address = in.next();
                System.out.print("Enter salary:");
                double salary = in.nextDouble();

                out.println(id + "\t" + name + "\t" + address + "\t" + salary);
            }
        } finally {
            out.close();
        }
    }

    private static void readData() throws FileNotFoundException {

        Scanner out = null;
        try {
            out = new Scanner(new FileInputStream("emp.doc"));
            while (out.hasNext()) {
                int id = out.nextInt();
                String name = out.next();
                String address = out.next();
                double salary = out.nextDouble();

                System.out.println("ID:" + id + "\tName:" + name + "\tAddress:" + address + "\tSalary:" + salary);
            }
        } finally {
            out.close();
        }

    }

    private static void readSalGreaterTwoK() throws FileNotFoundException {
        Scanner out = null;
        try {
            out = new Scanner(new FileInputStream("emp.doc"));
            while (out.hasNext()) {
                int id = out.nextInt();
                String name = out.next();
                String address = out.next();
                double salary = out.nextDouble();

                if (salary > 25000) {
                    System.out.println("ID:" + id + "\tName:" + name + "\tAddress:" + address + "\tSalary:" + salary);
                }
            }
        } finally {
            out.close();
        }
    }

}
