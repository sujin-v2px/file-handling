package javafilehandling;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;


public class QuestionThree {
    public static void main(String[] args) throws FileNotFoundException, IOException {
        FileReader fr=null;
        PrintWriter out=null;
        try{
            fr = new FileReader("q3.txt");
            out =new PrintWriter("q3op.txt");
            int c;
            while((c=fr.read())!=-1){
                out.write((char)c);
                out.write((char)c);
            }
        }finally{
            fr.close();
            out.close();
        }
        
        
    }
}
