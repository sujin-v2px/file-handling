package javafilehandling;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class QuestionFour {

    private static PrintWriter out = null;

    public static void main(String[] args) throws IOException {
        calculate();
    }

    public static void createMarkSheet() throws IOException {
        try {
            out = new PrintWriter(new FileWriter("q4.txt"));
            for (int i = 0; i < 20; i++) {
                int id = i;
                int math = (int) ((Math.random() * 100) + 1);
                int eng = (int) ((Math.random() * 100) + 1);
                int sci = (int) ((Math.random() * 100) + 1);
                int nep = (int) ((Math.random() * 100) + 1);
                int comp = (int) ((Math.random() * 100) + 1);

                out.println(id + "\t" + math + "\t" + eng + "\t" + sci + "\t" + nep + "\t" + comp);
            }
        } finally {
            out.close();
        }
    }

    public static void calculate() throws FileNotFoundException, IOException {
        Scanner in = null;
        try {
            in = new Scanner(new FileInputStream("q4.txt"));
            out = new PrintWriter(new FileWriter("q4op.txt"));
            while (in.hasNext()) {
                int id = in.nextInt();
                int s1 = in.nextInt();
                int s2 = in.nextInt();
                int s3 = in.nextInt();
                int s4 = in.nextInt();
                int s5 = in.nextInt();

                int total = (s1 + s2 + s3 + s4 + s5);
                float percent = (float) total / 5;
                String div;

                if (percent >= 80) {
                    div = "Distinction";
                } else if (percent < 80 && percent >= 60) {
                    div = "First Division";
                } else if (percent < 60 && percent >= 40) {
                    div = "Second Division";
                } else {
                    div = "Failed";
                }

                out.println("ID:" + id + "\tTotal:" + total + "\tPercentage:" + percent + "%\tDivision:" + div);
            }

        } finally {
            in.close();
            out.close();
        }
    }

}
